#include <cppos/scheduler.hh>

#include <charconv>

#include "freertos/FreeRTOS.h"

namespace
{
using namespace cppos;

exec_context *ctx[2] = {nullptr, nullptr};

void f_call_wrapper(nullary_function *f)
{
    (*f)();
}

void runTask(void *arg)
{
    auto tsk = static_cast<cppos::impl::task_block *>(arg);
    tsk->foo();
}
} // unnamed namespace

namespace cppos
{
void startContexts()
{
    // we assume that we still run on CPU0
    // first start tasks on CPU1
    ctx[1]->start();
    // now make sure that we can run to the end
    vTaskPrioritySet(NULL, configMAX_PRIORITIES-1);
    // start all tasks on this CPU
    ctx[0]->start();
    // now let them run
    vTaskPrioritySet(NULL, 1);
}

exec_context::exec_context(int coreId)
  : core(coreId)
  , curTaskCnt()
{
    ctx[coreId] = this;
}

void exec_context::startTask(impl::task_block &tb)
{
    std::array<char, 4> buf;
    char *end = std::to_chars(buf.data(),
                              buf.data() + buf.size(),
                              tb.id).ptr;
    *end = '\0';
    xTaskCreatePinnedToCore(runTask,
                            buf.data(),
                            tb.stackSize,
                            &tb,
                            tb.priority,
                            &(tb.frHandle),
                            core);
}

void exec_context::start()
{
    if (!started)
    {
        for (size_t i = 0; i != curTaskCnt; ++i)
        {
            startTask(tasks[i]);
        }
        started = true;
    }
}

} // namespace cppos
