// -*- C++ -*-
#ifndef _CPPOS_EXECUTION_INCLUDED
#define _CPPOS_EXECUTION_INCLUDED

#include <utility>
#include <concepts>
#include <type_traits>
#include <exception>
#include <functional>
#include <optional>
#include <tuple>

#include <tag_invoke.hh>

namespace std
{
namespace execution
{
template <typename Sched, std::invocable Foo>
void execute(Sched scheduler, Foo &&f)
{
    scheduler.execute(std::forward<Foo>(f));
}

namespace impl
{
template <class Foo, typename ArgT>
struct result_type
{
    typedef std::invoke_result_t<Foo, ArgT> type;
};

template <class Foo>
struct result_type<Foo, void>
{
    typedef std::invoke_result_t<Foo> type;
};

template <class Foo, typename ArgT>
using result_type_t = typename result_type<Foo, ArgT>::type;
} // namespace impl

template<class S>
concept scheduler =
    std::copy_constructible<std::remove_cvref_t<S>> &&
    std::equality_comparable<std::remove_cvref_t<S>>;

template<class S>
concept sender =
    std::move_constructible<std::remove_cvref_t<S>>;

template <sender Snd, typename Foo>
auto operator|(Snd&& s, Foo&& f)
{
    return forward<Foo>(f)(forward<Snd>(s));
}

struct set_value_t
{
    template <typename Recv, typename ...Ts>
    auto operator()(Recv &&r, Ts &&...args) const
    {
        return tag_invoke(set_value_t{},
                                std::forward<Recv>(r),
                                std::forward<Ts &&...>(args...));
    }
    template <typename Recv>
    auto operator()(Recv &&r) const
    {
        return tag_invoke(set_value_t{},
                          std::forward<Recv>(r));
    }
};
inline constexpr set_value_t set_value;

struct set_error_t
{
    template <typename Recv, typename Err>
    auto operator()(Recv &&r, Err &&e) const noexcept
    {
        return tag_invoke(set_error_t{},
                                std::forward<Recv>(r),
                                std::forward<Err>(e));
    }
};
inline constexpr set_error_t set_error;

struct set_done_t
{
    template <typename Recv>
    auto operator()(Recv &&r) const noexcept
    {
        return tag_invoke(set_done_t{}, std::move(r));
    }
};
inline constexpr set_done_t set_done;

struct start_t
{
    template <typename State>
    constexpr void operator()(State &s) const
    {
        return tag_invoke(start_t{}, s);
    }
};
inline constexpr start_t start;

template<class T, class E = std::exception_ptr>
concept receiver =
    std::move_constructible<std::remove_cvref_t<T>> &&
    std::constructible_from<std::remove_cvref_t<T>, T> &&
    requires (std::remove_cvref_t<T> &&t, E &&e)
    {
        { execution::set_done(std::move(t)) } noexcept;
        { execution::set_error(std::move(t), std::forward<E &&>(e)) } noexcept;
//        { execution::set_done(t) } noexcept;
//        { execution::set_error(t, std::forward<E &&>(e)) } noexcept;
    };

template<class T, class... Ts>
concept receiver_of =
    receiver<T> &&
    requires (std::remove_cvref_t<T> &&t, Ts &&...args)
    {
        execution::set_value(std::move(t), std::forward<Ts &&...>(args...));
    };

struct connect_t
{
    template <sender Snd, receiver Recv>
    auto operator()(Snd &&s, Recv &&r) const
    {
        return tag_invoke(connect_t{},
                          std::forward<Snd>(s),
                          std::forward<Recv>(r));
    }
};
inline constexpr connect_t connect;

template<class S, class R>
concept sender_to =
    sender<S> &&
    receiver<R> &&
    requires (S &&s, R &&r)
    {
        execution::connect(std::forward<S &&>(s),
                           std::forward<R &&>(r));
    };

template <typename ComplFoo>
struct get_completion_scheduler_t
{
    template <sender Snd>
    auto operator()(Snd s) const noexcept
    {
        return tag_invoke(*this, s);
    }
};
template <typename ComplFoo>
inline constexpr get_completion_scheduler_t<ComplFoo> get_completion_scheduler;


struct schedule_t
{
    template <scheduler SchedT>
    auto operator()(SchedT &&sched) const
    {
        return tag_invoke(schedule_t{}, std::forward<SchedT>(sched));
    }
};
inline constexpr schedule_t schedule;

struct then_t
{
    template <receiver InnerRecv, class Foo>
    struct then_receiver
    {
        InnerRecv r;
        Foo f;

        template <std::convertible_to<set_value_t> T,
                  std::convertible_to<then_receiver> ThenRec,
                  typename ...Ts>
        friend constexpr void tag_invoke(T,
                                         ThenRec &&self,
                                         Ts &&...args)
        {
            if constexpr (std::is_same_v<std::invoke_result_t<Foo, Ts...>, void>)
            {
                std::invoke(std::forward<Foo>(self.f),
                            std::forward<Ts &&...>(args...));
                set_value(std::move(self.r));
            }
            else
            {
                set_value(std::move(self.r),
                          std::invoke(std::forward<Foo>(self.f),
                                      std::forward<Ts &&...>(args...)));
            }
        }

        template <std::convertible_to<set_value_t> T,
                  std::convertible_to<then_receiver> ThenRec>
        friend constexpr void tag_invoke(T, ThenRec &&self)
        {
            set_value(std::move(self.r),
                      std::invoke(std::forward<Foo>(self.f)));
        }

        // forward other ops
        template <std::convertible_to<set_done_t> T>
        friend constexpr void tag_invoke(T, then_receiver &&self)
        {
            set_done(std::move(self.r));
        }

        template <std::convertible_to<set_error_t> T, typename Err>
        friend constexpr void tag_invoke(T, then_receiver const &self, Err &&e)
        {
            set_error(std::move(self.r), std::forward<Err>(e));
        }
    };

    template <sender InnerSnd, class Foo>
    struct then_sender
    {
        typedef typename InnerSnd::value_types in_type;
        typedef typename impl::result_type_t<Foo, in_type> value_types;

        InnerSnd s;
        Foo f;

        //template <class ThenSnd, receiver Recv>
        template <std::convertible_to<connect_t> T, class ThenSnd, class Recv>
            requires (std::is_same_v<std::decay_t<ThenSnd>, then_sender>)
        friend auto tag_invoke(T, ThenSnd &&self, Recv &&r)
        {
            return connect(std::forward<InnerSnd>(self.s),
                           then_receiver<Recv, Foo>{std::forward<Recv>(r),
                                                    std::forward<Foo>(self.f)});
        }

        template <typename ComplFoo>
        friend auto
        tag_invoke(get_completion_scheduler_t<ComplFoo>, then_sender s) noexcept {
            return get_completion_scheduler<ComplFoo>(s.s);
        }
    };

    //template <sender InnerSnd, class Foo>
    template <std::convertible_to<then_t> T, sender InnerSnd, class Foo>
    friend constexpr auto tag_invoke(T, InnerSnd &&s, Foo &&f)
    {
        return then_sender{std::forward<InnerSnd>(s), std::forward<Foo>(f)};
    }

    template <sender Snd, class Foo>
    sender auto operator()(Snd &&s, Foo &&f) const
    {
        return tag_invoke(then_t{},
                                std::forward<Snd>(s),
                                std::forward<Foo>(f));
    }

    template <class Foo> auto operator()(Foo &&f) const;
};
inline constexpr then_t then;

template <class Foo>
auto then_t::operator()(Foo &&f) const
{
    return [f = forward<Foo>(f)](sender auto &&s) mutable
    {
        return then(forward<decltype(s)>(s), move(f));
    };
}


namespace impl
{
template <typename T>
struct wait_result_type
{
    wait_result_type(T &&in)
      : res(std::forward<T>(in))
    {}

    T value()
    {
        return {};
    }

private:
    T res;
};

template <typename T>
auto make_wait_result(T &&v)
{
    std::optional<std::tuple<std::decay_t<T>>> res = std::forward<T>(v);
    return res;
}


template <typename T>
struct ResultState
{
    T value;
};

template <>
struct ResultState<void>
{
    int value = 0;
};


template <typename T>
struct WaitRecv
{
    WaitRecv(ResultState<T> &res)
      : result(res)
    {
    }
    WaitRecv()
    {
    }

    template <std::convertible_to<set_value_t> TagT, typename VT>
    friend constexpr void tag_invoke(TagT, WaitRecv &&self, VT &&v)
    {
        self.result.value = std::forward<VT>(v);
    }
    template <std::convertible_to<set_value_t> TagT>
    friend constexpr void tag_invoke(TagT, WaitRecv &&self)
    {
    }

    friend constexpr void tag_invoke(set_done_t, WaitRecv &&) noexcept
    {
    }

    template <typename Err>
    friend constexpr void tag_invoke(set_error_t, WaitRecv const &, Err &&e)
    {
        // we ignore for now
    }

    ResultState<T> &result;
};

} // namespace impl

struct sync_wait_t
{
    template <class Snd>
    auto operator()(Snd &&s) const
    {
        auto sch = get_completion_scheduler<set_value_t>(std::forward<Snd>(s));
        return tag_invoke(sync_wait_t{},
                          sch,
                          std::forward<Snd>(s));
    }

    auto operator()() const;
};
inline constexpr sync_wait_t sync_wait;

inline auto sync_wait_t::operator()() const
{
    return [] (sender auto &&s) mutable
           {
               return sync_wait(forward<decltype(s)>(s));
           };
}
} // namespace execution
} // namespace std
#endif /* _CPPOS_EXECUTION_INCLUDED */
