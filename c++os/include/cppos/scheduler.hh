#ifndef _SCHEDULER_HH_INCLUDED
#define _SCHEDULER_HH_INCLUDED
#include "systraits.hh"

#include <cstdint>
#include <array>

#include "freertos/FreeRTOS.h"
#include <freertos/task.h>

#include "helpers.tt"


namespace cppos
{
namespace impl
{
struct task_block
{
    nullary_function foo;
    TaskHandle_t frHandle;
    size_t stackSize;
    uint8_t id;
    uint8_t priority;
};
} // namespace impl

/**
 * Correct startup sequence in the general case is pretty hard.
 * So we simply require that the program only uses C++OS mechanisms
 * to start new tasks.
 * I.e. no direct calls to xTaskCreate*.
 */
void startContexts();

class exec_context
{
public:
    template <uint8_t TaskId,
              size_t StackSize = sys_traits::default_stack_size,
              uint8_t Priority = sys_traits::default_priority>
    struct scheduler
    {
        scheduler(exec_context *ctx)
          : context{ctx}
        {}

        template <std::invocable Foo>
        void execute(Foo &&f)
        {
            context->add_static(std::forward<Foo>(f),
                                TaskId, StackSize, Priority);
        }

        exec_context *context;
    };

    exec_context(int coreId = -1);
    exec_context(exec_context &&) = delete;

    template <std::invocable Foo>
    void add_static(Foo &&f,
                    uint8_t id,
                    size_t stack_size = sys_traits::default_stack_size,
                    uint8_t prio = sys_traits::default_priority)
    {
        impl::task_block &t = tasks[curTaskCnt];
        t.foo = std::forward<Foo>(f);
        t.frHandle = nullptr;
        t.stackSize = stack_size;
        t.id = id;
        t.priority = prio;

        ++curTaskCnt;

        if (started)
        {
            startTask(t);
        }
    }

    template <uint8_t TaskId,
              size_t StackSize = sys_traits::default_stack_size,
              uint8_t Priority = sys_traits::default_priority>
    auto get_scheduler()
    {
        return scheduler<TaskId, StackSize, Priority>(this);
    }

private:
    friend void startContexts();

    void start();
    void startTask(impl::task_block &);

    int core;
    std::array<impl::task_block, sys_traits::static_task_count> tasks;
    size_t curTaskCnt;
    bool started = false;
};
} // namespace cppos
#endif /* _SCHEDULER_HH_INCLUDED */
