#ifndef _HELPERS_HH_INCLUDED
#define _HELPERS_HH_INCLUDED

#include <cstdint>
#include <array>
#include <bit>
#include <concepts>

#include <functional>

namespace cppos
{
typedef std::function<void()> nullary_function;
} // namespace cppos
#endif /* _HELPERS_HH_INCLUDED */
