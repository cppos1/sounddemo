#ifndef LOOP_EXEC_HH_INCLUDED
#define LOOP_EXEC_HH_INCLUDED

#include "execution.hh"
#include <thread>
#include <chrono>

namespace cppos
{
namespace exec = std::execution;
using std::execution::receiver;
using std::execution::sender;

using namespace std::literals;


template <typename Sch>
class LoopExecutor
{
public:
    class Scheduler
    {
        friend class LoopExecutor<Sch>;

        LoopExecutor<Sch> *ctx;
        nullary_function *blockFoo;

        struct OpBase
        {
            void (*f)(OpBase *);
        };

        template <receiver Recv>
        struct Op : OpBase
        {
            template <receiver R>
            explicit Op(R &&r, LoopExecutor<Sch> *ctx_, nullary_function *block)
              : ctx(ctx_)
              , rec(std::forward<R>(r))
              , blockFoo(block)
            {
                this->f = [] (OpBase *self)
                {
                    auto me = static_cast<Op *>(self);
                    exec::set_value(Recv(me->rec)); // we need an rvalue
                };
            }

            friend void tag_invoke(exec::start_t, Op &o) noexcept
            {
                // activate the event
                o.ctx->startTask(&o, o.blockFoo);
            }

            template <sender Snd>
            using SndType = typename std::decay_t<Snd>::value_types;

            LoopExecutor<Sch> *ctx;
            Recv rec;
            nullary_function *blockFoo;
        };

    public:
        struct Sender
        {
            typedef void value_types;

            Sender() = delete;
            Sender(LoopExecutor<Sch> *ex, nullary_function *block)
              : ctx(ex)
              , blockFoo(block)
            {
            }

            LoopExecutor<Sch> *ctx;
            nullary_function *blockFoo;
        };

        Scheduler() = delete;
        Scheduler(Scheduler const &) = default;
        Scheduler(Scheduler &&) = default;

        Scheduler(LoopExecutor<Sch> *ex, nullary_function *f)
          : ctx(ex)
          , blockFoo(f)
        {
        }

        template <std::convertible_to<exec::connect_t> T, receiver Recv>
        friend constexpr auto tag_invoke(T , Sender &&s, Recv &&r)
        {
            return Op<std::decay_t<Recv>>(std::forward<Recv>(r), s.ctx, s.blockFoo);
        }

        template <std::convertible_to<exec::schedule_t> TagT>
        friend Sender constexpr tag_invoke(TagT, Scheduler s)
        {
            return {s.ctx, s.blockFoo};
        }

        template <typename ComplFoo,
                  std::convertible_to<exec::get_completion_scheduler_t<ComplFoo>> TagT>
        friend Scheduler
        tag_invoke(TagT, Sender s) noexcept
        {
            return {s.ctx, s.blockFoo};
        }

        template <std::convertible_to<forEver_t> T, sender Snd>
        friend void
        tag_invoke(T, Scheduler sch, Snd &&s) noexcept
        {
            auto opState = exec::connect(std::forward<Snd>(s),
                                   impl::VoidRecv());
            exec::start(opState);

            // to keep the opState alive, we must not return!
            while (true)
            {
                std::this_thread::sleep_for(5s);
            }
        }

        bool operator==(Scheduler const &rhs) const
        {
            return rhs.ctx == ctx;
        }
    };

    LoopExecutor(Sch s)
      : agent(s)
    {
    }

    Scheduler scheduler(nullary_function *f)
    {
        return {this, f};
    }

    void startTask(Scheduler::OpBase *op, nullary_function *block)
    {
        exec::execute(agent,
                      [this, op, block]
                      {
                          while (true)
                          {
                              (*block)();
                              op->f(op);
                          }
                      });
        printf("LTask started\n");
    }

private:
    Sch agent;
};
} // namespace cppos
#endif /* LOOP_EXEC_HH_INCLUDED */
