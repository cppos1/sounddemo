#ifndef CPPOS_EXECUTION_HH_INCLUDED
#define CPPOS_EXECUTION_HH_INCLUDED

#include <execution>

#include <span>
#include <cstring>

#include <freertos/FreeRTOS.h>
#include <freertos/ringbuf.h>

namespace cppos
{
namespace exec = std::execution;

using exec::operator|;

struct bufferedTransfer_t;

namespace impl
{


struct VoidRecv
{
    VoidRecv()
    {
    }

    template <std::convertible_to<exec::set_value_t> TagT,
              std::convertible_to<VoidRecv> VRec>
    friend constexpr void tag_invoke(TagT, VRec &&self)
    {
    }

    template <std::convertible_to<exec::set_done_t> TagT>
    friend constexpr void tag_invoke(TagT, VoidRecv &&) noexcept
    {
        // we ignore for now
    }

    template <std::convertible_to<exec::set_error_t> TagT, typename Err>
    friend constexpr void tag_invoke(TagT, VoidRecv const &, Err &&e)
    {
        // we ignore for now
    }
};
} // namespace impl

struct bufferedTransfer_t
{
    template <typename QValT>
    struct BufTransReceiver1
    {
        BufTransReceiver1(RingbufHandle_t q)
          : out(q)
        {
        }

        RingbufHandle_t out;

        template <std::convertible_to<exec::set_value_t> T,
                  std::convertible_to<BufTransReceiver1> BTRec,
            std::convertible_to<std::span<std::byte>> ValT>
        friend void tag_invoke(T,
                               BTRec &&self,
                               ValT buf)
        {
            void *start;
            xRingbufferSendAcquire(self.out, &start, buf.size(), portMAX_DELAY);
            memcpy(start, buf.data(), buf.size());
            xRingbufferSendComplete(self.out, start);
        }

        template <std::convertible_to<exec::set_done_t> T>
        friend void tag_invoke(T, BufTransReceiver1 &&self) noexcept
        {
            // ignored
        }

        template <std::convertible_to<exec::set_error_t> T, typename Err>
        friend constexpr void tag_invoke(T, BufTransReceiver1 const &self, Err &&e) noexcept
        {
            // we ignore it, as we can't push an error into the queue
        }
    };

    template <exec::receiver InnerRecv>
    struct BufTransReceiver2
    {
        BufTransReceiver2(InnerRecv &&r_, RingbufHandle_t &q)
          : r(r_)
          , in(q)
        {
        }

        InnerRecv r;
        RingbufHandle_t &in;

        // we only provide the overload for void
        // we ignore close for now
        template <std::convertible_to<exec::set_value_t> TagT,
                  std::convertible_to<BufTransReceiver2> BTRec>
        friend constexpr void tag_invoke(TagT, BTRec &&self)
        {
            // we know now that a value is available
            size_t size;
            void *p = xRingbufferReceive(self.in, &size, portMAX_DELAY);
            std::byte *start = static_cast<std::byte *>(p);
            std::span<std::byte> buf(static_cast<std::byte *>(p),
                                           size);
            exec::set_value(self.r, buf);
            vRingbufferReturnItem(self.in, start);
        }

        template <std::convertible_to<exec::set_done_t> T>
        friend void tag_invoke(T, BufTransReceiver2 &&self) noexcept
        {
            // nobody should call this, but it's required for the concept
        }

        template <std::convertible_to<exec::set_error_t> T, typename Err>
        friend constexpr void tag_invoke(T, BufTransReceiver2 const &self, Err &&e) noexcept
        {
            // nobody should call this, but it's required for the concept
        }
    };

    template <exec::sender InnerSnd, class OtherSched>
    struct BufTransSender
    {
        typedef typename InnerSnd::value_types in_type;
        typedef typename InnerSnd::value_types value_types;

        template <class Recv>
        struct CombinedState
        {
            decltype(exec::connect(std::declval<InnerSnd>(),
                                   std::declval<BufTransReceiver1<in_type>>())) op1;
            decltype(exec::connect(std::declval<decltype(exec::schedule(std::declval<OtherSched>()))>(),
                                   std::declval<BufTransReceiver2<Recv>>())) op2;

            friend void tag_invoke(exec::start_t, CombinedState &self) noexcept
            {
                exec::start(self.op1);
                exec::start(self.op2);
            }
        };

        InnerSnd s;
        OtherSched sch;
        RingbufHandle_t in;

        template <std::convertible_to<exec::connect_t> T, class BTSnd, class Recv>
        requires (std::is_same_v<std::decay_t<BTSnd>, BufTransSender>)
        friend auto tag_invoke(T, BTSnd &&self, Recv &&r)
        {
            return CombinedState<Recv>{
                exec::connect(std::forward<InnerSnd>(self.s),
                              BufTransReceiver1<in_type>(self.in)),
                exec::connect(exec::schedule(self.sch),
                              BufTransReceiver2<Recv>(std::forward<Recv>(r), self.in))};
        }

        template <std::convertible_to<exec::get_completion_scheduler_t<exec::set_value_t>> TagT>
        friend OtherSched tag_invoke(TagT, BufTransSender s) noexcept
        {
            return s.sch;
        }
    };

    template <exec::sender InnerSnd, class OtherSched>
    friend constexpr auto tag_invoke(bufferedTransfer_t,
                                     InnerSnd &&s,
                                     RingbufHandle_t q,
                                     OtherSched &&sch)
    {
        return BufTransSender{std::forward<InnerSnd>(s),
                              std::forward<OtherSched>(sch),
                              q};
    }

    template <exec::sender Snd, class OtherSched>
    exec::sender auto operator()(Snd &&s,
                                 RingbufHandle_t q,
                                 OtherSched &&sch) const
    {
        return tag_invoke(bufferedTransfer_t{},
                          std::forward<Snd>(s),
                          q,
                          std::forward<OtherSched>(sch));
    }

    template <class OtherSched> auto operator()(RingbufHandle_t q,
                                                OtherSched &&sch) const;
};
inline constexpr bufferedTransfer_t bufferedTransfer;

template <class OtherSched>
auto bufferedTransfer_t::operator()(RingbufHandle_t q,
                                    OtherSched &&sch) const
{
    return [q, sch = std::forward<OtherSched>(sch)]
           (exec::sender auto &&s) mutable
           {
               return bufferedTransfer(std::forward<decltype(s)>(s),
                                       q,
                                       std::move(sch));
    };
}


struct forEver_t
{
    template <class Snd>
    auto operator()(Snd &&s) const
    {
        auto sch = exec::get_completion_scheduler<exec::set_value_t>(std::forward<Snd>(s));
        return tag_invoke(forEver_t{},
                          sch,
                          std::forward<Snd>(s));
    }

    auto operator()() const;
};
inline constexpr forEver_t forEver;

inline auto forEver_t::operator()() const
{
    return [] (exec::sender auto &&s) mutable
           {
               return forEver(std::forward<decltype(s)>(s));
           };
}
} // namespace cppos
#endif /* CPPOS_EXECUTION_HH_INCLUDED */
