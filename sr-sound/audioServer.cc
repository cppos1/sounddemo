
#include "audioServer.hh"

#include <array>

#include <lwip/sockets.h>

#include <execution>
#include <cppos/execution.hh>

void readAll(int sock, void *buf, size_t size)
{
    auto rBuf = static_cast<uint8_t *>(buf);
    size_t offset = 0;
    while (offset < size)
    {
        int r = read(sock, rBuf + offset, size - offset);
        offset += r;
    }
}

std::span<std::byte> receiveWireChunk(int sock, std::span<std::byte> buf)
{
    SnapCast::BaseAligned baseMsg;
    do
    {
        readAll(sock, &baseMsg.msg, sizeof(baseMsg.msg));
        readAll(sock, buf.data(), baseMsg.msg.size);
    } while (baseMsg.msg.type != SnapCast::wireChunk);

    return {buf.data(), baseMsg.msg.size};
}
