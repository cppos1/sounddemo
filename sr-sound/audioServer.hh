#ifndef AUDIO_SERVER_HH_INCLUDED
#define AUDIO_SERVER_HH_INCLUDED

#include <span>
#include <cstdint>
#include <execution>

#include "lwip/sockets.h"

#include <cppos/execution.hh>
#include <cppos/helpers.hh>

extern SemaphoreHandle_t startDecode;

namespace
{
constexpr int packetsToWait = 30;
int packetsGot = 0;
bool decodeStarted = false;
} // unnamed namespace

namespace SnapCast
{
// the protocol is little-endian and we're little-endian
enum MsgType : uint16_t
{
    base = 0,
    codecHeader = 1,
    wireChunk = 2,
    serverSettings = 3,
    time = 4,
    hello = 5,
    streamTags = 6,
};

struct [[gnu::packed]] TimeStruct
{
    int32_t sec;
    int32_t uSec;
};

// !!! be aware of the misalignment of the 32-bit values !!!
struct [[gnu::packed]] Base
{
    MsgType type;
    uint16_t id;
    uint16_t refersTo;
    TimeStruct sent;
    TimeStruct recv;
    uint32_t size;
};
// for aligned access
struct [[gnu::packed]] BaseAligned
{
    uint16_t padding;
    Base msg;
};

struct [[gnu::packed]] Hello
{
    uint32_t size;
    char jsonString[1024];
};

struct [[gnu::packed]] WireChunk
{
    TimeStruct stamp;
    uint32_t size;
    uint8_t payload[4]; // actually `size`
};

#define HELLO_STR \
    R"({"Arch":"xtensa",)" \
    R"("ClientName":"Soundclient",)" \
    R"("HostName":"esp32spkr",)" \
    R"("ID":"00:11:22:33:44:55",)" \
    R"("Instance":1,)" \
    R"("MAC":"00:11:22:33:44:55",)" \
    R"("OS":"FreeRTOS",)" \
    R"("SnapStreamProtocolVersion": 2,)" \
    R"("Version": "0.0.1"})"

inline Hello helloMsg =
{
    .size = sizeof(HELLO_STR) - 1, // no null-byte
    .jsonString = HELLO_STR,
};
} // namespace SnapCast

constexpr size_t netBufSize = 5000;

void readAll(int sock, void *buf, size_t size);
std::span<std::byte> receiveWireChunk(int sock, std::span<std::byte> buf);

namespace exec = std::execution;

template <class Ctx>
class AudioServer
{
public:
    template <exec::receiver InnerRecv>
    struct TriggerRecv
    {
        TriggerRecv(InnerRecv &&r_, int netSock)
          : r(r_)
          , sock(netSock)
        {
        }

        template <std::convertible_to<exec::set_value_t> TagT,
                  std::convertible_to<TriggerRecv> TRec>
        friend constexpr void tag_invoke(TagT, TRec &&self)
        {
            std::span<std::byte> chunk(receiveWireChunk(self.sock, netBuf));
            exec::set_value(self.r, chunk);

            if (!decodeStarted)
            {
                ++packetsGot;
                if (packetsGot == packetsToWait)
                {
                    xSemaphoreGive(startDecode);
                    decodeStarted = true;
                    printf("Starting decode\n");
                }
            }
        }

        template <std::convertible_to<exec::set_done_t> TagT>
        friend constexpr void tag_invoke(TagT, TriggerRecv &&) noexcept
        {
            // we ignore for now
        }

        template <std::convertible_to<exec::set_error_t> TagT, typename Err>
        friend constexpr void tag_invoke(TagT, TriggerRecv const &, Err &&e)
        {
            // we ignore for now
        }

    private:
        InnerRecv r;
        int sock;
    };

    template <exec::sender InnerSnd>
    struct NetSender
    {
        typedef typename InnerSnd::value_types in_type;
        typedef std::span<std::byte> value_types;

        InnerSnd s;
        int sock;

        template <std::convertible_to<exec::connect_t> T, class Snd, class Recv>
        requires (std::is_same_v<std::decay_t<Snd>, NetSender>)
        friend auto tag_invoke(T, Snd &&self, Recv &&r)
        {
            return exec::connect(std::forward<InnerSnd>(self.s),
                                 TriggerRecv<Recv>{std::forward<Recv>(r),
                                                   self.sock});
        }

        template <typename ComplFoo>
        friend auto
        tag_invoke(exec::get_completion_scheduler_t<ComplFoo>, NetSender s) noexcept {
            return exec::get_completion_scheduler<ComplFoo>(s.s);
        }
    };

    /**
     * @brief Opens the socket and does the whole start handshake:
     *         - send the Hello message
     *         - read (and discard) ServerSettings
     *         - read (and discard) StreamTags
     *         - read (and discard) CodecHeader
     */
    AudioServer(Ctx &runCtx, char const *srvAddrStr, uint16_t port)
      : selectWait([this]
                   {
                       fd_set rfd;
                       FD_ZERO(&rfd);
                       FD_SET(sock, &rfd);
                       select(sock + 1, &rfd,
                              nullptr, nullptr, nullptr);
                   })
      , sch(runCtx.scheduler(&selectWait))
    {
        sock = lwip_socket(AF_INET, SOCK_STREAM, 0);

        sockaddr_in srvAddr;
        srvAddr.sin_family = AF_INET;
        lwip_inet_pton(AF_INET, srvAddrStr, &(srvAddr.sin_addr.s_addr));
        srvAddr.sin_port = htons(port);
        lwip_connect(sock, (sockaddr *)&srvAddr, sizeof(srvAddr));

        timeval now;
        gettimeofday(&now, nullptr);
        SnapCast::BaseAligned baseMsg =
            {
                0,
                {
                    SnapCast::hello,
                    0, 0,
                    {now.tv_sec, now.tv_usec},
                    {0, 0},
                    SnapCast::helloMsg.size + sizeof(SnapCast::helloMsg.size),
                },
            };

        write(sock, &baseMsg.msg, sizeof(baseMsg.msg));
        write(sock, &SnapCast::helloMsg, baseMsg.msg.size);

        // read and discard everything until we get a CodecHeader
        do
        {
            readAll(sock, &baseMsg.msg, sizeof(baseMsg.msg));
            readAll(sock, netBuf.data(), baseMsg.msg.size);
        } while (baseMsg.msg.type != SnapCast::codecHeader);

        printf("Network handshake done\n");
    }

    /**
     * @brief Provide a sender that reads one packet and forwards it
     */
    auto readPacket()
    {
        return NetSender{exec::schedule(sch), sock};
    }

private:
    cppos::nullary_function selectWait;
    typename Ctx::Scheduler sch;
    int sock;

    static std::array<std::byte, netBufSize> netBuf;
};
template <class Ctx> std::array<std::byte, netBufSize> AudioServer<Ctx>::netBuf;
#endif /* AUDIO_SERVER_HH_INCLUDED */
