#ifndef DECODE_HH_INCLUDED
#define DECODE_HH_INCLUDED

#define FIXED_POINT 1
#include <opus.h>
#include <opus_types.h>
#include <span>
#include <cstddef>

constexpr opus_int32 sampleRate = 48000; // 1 sample ~= 20.83us
constexpr int channels = 2;

class OpusAudioDecoder
{
public:
    OpusAudioDecoder();
    ~OpusAudioDecoder();

    std::span<std::byte> decode(std::span<std::byte> opusFrame);

private:
    OpusDecoder *decoder;
};
#endif /* DECODE_HH_INCLUDED */
