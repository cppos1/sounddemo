
#include "decode.hh"
#include "audioServer.hh"

#define FIXED_POINT 1
#include <opus.h>

#include <cstdio>
#include <bit>
#include <cstring>
#include <array>

#include <freertos/FreeRTOS.h>
#include <freertos/ringbuf.h>
#include <freertos/semphr.h>

#include <esp_heap_caps.h>

#include <silk/control.h>


extern RingbufHandle_t inBuf;
extern RingbufHandle_t outBuf;
extern SemaphoreHandle_t startDecode;

extern char *global_stack;
extern char *global_stack_end;
extern char *scratch_ptr;

namespace
{
// server setting is 20ms, but the server may have some leeway
constexpr int maxFrameLenMs = 25;
constexpr int maxFrameSamples = maxFrameLenMs * (sampleRate / 1000);

std::array<opus_int16, maxFrameSamples * 4> decodeBuf;

// with ESP-IDF, BSS data can only go to SRAM 2
// SRAM 1 can only be used as heap (malloc)
//std::array<char, GLOBAL_STACK_SIZE> opusStack;

constexpr size_t opusDecoderSize = 26496;
std::array<std::byte, opusDecoderSize> decoderBuf;
} // unnamed namespace

OpusAudioDecoder::OpusAudioDecoder()
{
    int opusSize = opus_decoder_get_size(2);
    assert(opusSize >= opusDecoderSize);

    size_t maxBlockSize = heap_caps_get_largest_free_block(MALLOC_CAP_32BIT);
    printf("Max size: %u\n", maxBlockSize);
    decoder = std::bit_cast<OpusDecoder *>(decoderBuf.data());
    global_stack = static_cast<char *>(malloc(GLOBAL_STACK_SIZE));
    assert(global_stack != NULL);
    scratch_ptr = global_stack;
    global_stack_end = global_stack + GLOBAL_STACK_SIZE;
    printf("Init decoder...\n");
    int err = opus_decoder_init(decoder, sampleRate, channels);
    assert(err == OPUS_OK);
    printf("Init decoder done\n");
}

OpusAudioDecoder::~OpusAudioDecoder()
{
    // we don't clean up
}

std::span<std::byte> OpusAudioDecoder::decode(std::span<std::byte> opusFrame)
{
    using SnapCast::WireChunk;

    // we wait until the ring buffer is filled to about half
    printf("Starting decode...\n");

    while (true)
    {
        size_t size;
        void const *start = xRingbufferReceive(inBuf, &size, portMAX_DELAY);
        WireChunk const *chunk = std::bit_cast<WireChunk const *>(start);

        int samples = opus_decode(decoder,
                                  chunk->payload, chunk->size,
                                  decodeBuf.data(), maxFrameSamples,
                                  0);
        vRingbufferReturnItem(inBuf, const_cast<void *>(start));
        assert(samples >= 0);

        void *out;
        xRingbufferSendAcquire(outBuf, &out, samples * 4, portMAX_DELAY);
        memcpy(out, decodeBuf.data(), samples * 4);

        xRingbufferSendComplete(outBuf, out);
    }
}
