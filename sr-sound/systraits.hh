#ifndef SYSTRAITS_HH_INCLUDED
#define SYSTRAITS_HH_INCLUDED

#include <cstdint>

namespace cppos
{
struct sys_traits
{
    static constexpr int static_task_count = 5;
    static constexpr int dynamic_task_count = 2;
    static constexpr int default_stack_size = 2680;
    static constexpr int default_stack_guard_size = 4;
    static constexpr uint32_t default_stack_guard_value = 0xdead66aa;
    static constexpr int default_priority = 10;
    static constexpr int max_function_size = 12;
    static constexpr int min_stack_size = 660;
    static constexpr int main_stack_size = 980;
    //static constexpr uint32_t sys_timer_value = 168000;
    //static constexpr uint32_t stack_start = 0x;
    //static constexpr uint32_t stack_end = 0x;
};

struct port_traits
{
    // the scheduler doesn't need to know about the time slice
    // it's the port that need to handle this
    static constexpr int time_slice_ms = 1;
};
} // namespace cppos
#endif /* SYSTRAITS_HH_INCLUDED */
