// snapcast client

#include <cppos/scheduler.hh>

#include <execution>
#include <chrono>
#include <thread>
#include <charconv>

#include <cppos/loop-exec.hh>

#include <driver/i2s_std.h>

#include <sys/param.h>
#include <freertos/FreeRTOS.h>
#include <freertos/event_groups.h>
#include <freertos/ringbuf.h>
#include "esp_system.h"
#include "esp_wifi.h"
#include "esp_event.h"
#include "esp_netif.h"
#include "esp_log.h"
#include "nvs_flash.h"

#include "lwip/err.h"
#include "lwip/sockets.h"
#include "lwip/sys.h"
#include <lwip/netdb.h>

#include <array>
#include <cstdint>
#include <string_view>
#include <atomic>

#include "audioServer.hh"
#include "decode.hh"

#define AS_STRING_IMPL(x) #x
#define AS_STRING(x) AS_STRING_IMPL(x)

RingbufHandle_t inBuf;
RingbufHandle_t outBuf;
SemaphoreHandle_t startDecode;

namespace
{
constexpr char TAG[] = "net";

constexpr i2s_port_t i2sPort = I2S_NUM_0;
constexpr gpio_num_t bckIo = GPIO_NUM_23;
constexpr gpio_num_t wsIo = GPIO_NUM_22;
constexpr gpio_num_t doIo = GPIO_NUM_21;
constexpr uint32_t i2sDmaNum = 6;
constexpr uint32_t i2sDmaFrames = 900; // ~20ms, i.e. ~1 WireChunk

// our ring buffer can hold about the same again as the DMA buffers
// for each chunk the ring buffer has an 8-byte header
constexpr size_t outBufSize = i2sDmaNum * (i2sDmaFrames * 4 + 8);
constexpr size_t inBufSize = 80000;

constexpr uint32_t startPlayEv = 0x1;
constexpr uint32_t wifiConnectedEv = 0x1;
constexpr uint32_t wifiFailedEv = 0x2;
constexpr int maxWifiRetries = 5;
constexpr char wifiSsid[] = AS_STRING(WIFI_SSID);
constexpr char wifiPass[] = AS_STRING(WIFI_PASS);

constexpr char srvAddr[] = "192.168.1.4";
constexpr uint16_t audioPort = 1704;

i2s_chan_handle_t tx = nullptr;
EventGroupHandle_t wifiEv;

std::atomic<bool> stopAudio{false};

uint32_t inBufStore[inBufSize / 4]; // 32-bit aligned
StaticRingbuffer_t inRingBufAdm;
uint32_t outBufStore[outBufSize / 4]; // 32-bit aligned
StaticRingbuffer_t outRingBufAdm;

uint32_t dbgCntRecv;
uint32_t dbgCntPlay;


/**
 * @brief I2S init.
 */

esp_err_t i2sInit()
{
    i2s_chan_config_t txCfg =
        {
            .id = i2sPort,
            .role = I2S_ROLE_MASTER,
            .dma_desc_num = i2sDmaNum,
            .dma_frame_num = i2sDmaFrames,
            .auto_clear = true,
        };
    ESP_ERROR_CHECK(i2s_new_channel(&txCfg, &tx, nullptr));

    i2s_std_config_t i2sCfg =
    {
        .clk_cfg = I2S_STD_CLK_DEFAULT_CONFIG(sampleRate),
        .slot_cfg
            = I2S_STD_MSB_SLOT_DEFAULT_CONFIG(I2S_DATA_BIT_WIDTH_16BIT,
                                              I2S_SLOT_MODE_STEREO),
        .gpio_cfg = {
            .mclk = I2S_GPIO_UNUSED,
            .bclk = bckIo,
            .ws = wsIo,
            .dout = doIo,
            .din = I2S_GPIO_UNUSED,
            .invert_flags = {
                .mclk_inv = false,
                .bclk_inv = false,
                .ws_inv = false,
            },
        },
    };

    ESP_ERROR_CHECK(i2s_channel_init_std_mode(tx, &i2sCfg));
    ESP_ERROR_CHECK(i2s_channel_enable(tx));
    return ESP_OK;
}

void send2I2s(std::span<std::byte> frame)
{
        size_t offset = 0;
        std::byte const *start = frame.data();
        size_t size = frame.size();

        while (offset < size)
        {
            size_t bytesDone;
            i2s_channel_write(tx,
                              start + offset,
                              size - offset,
                              &bytesDone,
                              portMAX_DELAY);
            offset += bytesDone;
        }
}

void wifiEvHandler(void *,
                   esp_event_base_t base,
                   int32_t id,
                   void *data)
{
    static int retryCnt = 0;
    printf("Retries: %d, ID: %d\n", retryCnt, id);

    if (base == WIFI_EVENT && id == WIFI_EVENT_STA_START)
    {
        esp_wifi_connect();
    }
    else if (base == WIFI_EVENT && id == WIFI_EVENT_STA_DISCONNECTED)
    {
        if (retryCnt < maxWifiRetries)
        {
            ESP_LOGI(TAG, "retry to connect WIFI");
            ++retryCnt;
            esp_wifi_connect();
        }
        else
        {
            xEventGroupSetBits(wifiEv, wifiFailedEv);
        }
    }
    else if (base == IP_EVENT && id == IP_EVENT_STA_GOT_IP)
    {
        ip_event_got_ip_t* event = (ip_event_got_ip_t*) data;
        ESP_LOGI(TAG, "got ip:" IPSTR, IP2STR(&event->ip_info.ip));
        retryCnt = 0;
        xEventGroupSetBits(wifiEv, wifiConnectedEv);
    }
}

void wifiClientInit()
{
    printf("Start connect ...\n");
    wifiEv = xEventGroupCreate();

    ESP_ERROR_CHECK(esp_netif_init());

    esp_netif_create_default_wifi_sta();

    wifi_init_config_t cfg = WIFI_INIT_CONFIG_DEFAULT();
    ESP_ERROR_CHECK(esp_wifi_init(&cfg));

    ESP_ERROR_CHECK(esp_event_handler_register(WIFI_EVENT,
                                               ESP_EVENT_ANY_ID,
                                               &wifiEvHandler,
                                               NULL));
    ESP_ERROR_CHECK(esp_event_handler_register(IP_EVENT,
                                               IP_EVENT_STA_GOT_IP,
                                               &wifiEvHandler,
                                               NULL));

    wifi_config_t wifiCfg = {
        .sta = {
            .ssid = AS_STRING(WIFI_SSID),
            .password = AS_STRING(WIFI_PASS),
            //.threshold.authmode = WIFI_AUTH_WPA2_PSK,
        },
    };
    ESP_ERROR_CHECK(esp_wifi_set_mode(WIFI_MODE_STA));
    ESP_ERROR_CHECK(esp_wifi_set_config(WIFI_IF_STA, &wifiCfg));
    ESP_ERROR_CHECK(esp_wifi_start());
    printf("Connect started...\n");

    EventBits_t bits = xEventGroupWaitBits(wifiEv,
                                           wifiConnectedEv | wifiFailedEv,
                                           pdFALSE,
                                           pdFALSE,
                                           portMAX_DELAY);

    if (bits & wifiConnectedEv)
    {
        ESP_LOGI(TAG, "connected to SSID:%s", wifiSsid);
    }
    else if (bits & wifiFailedEv)
    {
        ESP_LOGI(TAG, "Failed to connect to SSID:%s", wifiSsid);
    }
    else
    {
        ESP_LOGE(TAG, "UNEXPECTED EVENT");
    }
}

uint8_t execCtx0Buf[sizeof(cppos::exec_context)];
uint8_t execCtx1Buf[sizeof(cppos::exec_context)];

cppos::exec_context *ctx0 = nullptr;
cppos::exec_context *ctx1 = nullptr;
} // unnamed namespace
std::span<std::byte> dummy();

extern "C" esp_err_t app_main()
{
    printf("Start...\n");

    // nvs_flash_init() currently hangs
    // maybe a C++ problem or linker problem (function not in IRAM)
    // for now we disabled use of NVS in sdkconfig.h
#if 0
    esp_err_t flRet = nvs_flash_init();
    if (flRet == ESP_ERR_NVS_NO_FREE_PAGES ||
        flRet == ESP_ERR_NVS_NEW_VERSION_FOUND)
    {
        printf("flash_init ret: %d\n", flRet);
        ESP_ERROR_CHECK(nvs_flash_erase());
        flRet = nvs_flash_init();
    }
    ESP_ERROR_CHECK(flRet);
#endif

    printf("SSID: %s\n", wifiSsid);

    ESP_ERROR_CHECK(esp_netif_init());
    ESP_ERROR_CHECK(esp_event_loop_create_default());

    wifiClientInit();

    i2sInit();

    inBuf = xRingbufferCreateStatic(inBufSize,
                                    RINGBUF_TYPE_NOSPLIT,
                                    (uint8_t *)inBufStore,
                                    &inRingBufAdm);
    outBuf = xRingbufferCreateStatic(outBufSize,
                                     RINGBUF_TYPE_NOSPLIT,
                                     (uint8_t *)outBufStore,
                                     &outRingBufAdm);

    QueueSetHandle_t opusWaiter = xQueueCreateSet(100);
    configASSERT(opusWaiter != nullptr);
    xRingbufferAddToQueueSetRead(inBuf, opusWaiter);

    QueueSetHandle_t pcmWaiter = xQueueCreateSet(100);
    configASSERT(pcmWaiter != nullptr);
    xRingbufferAddToQueueSetRead(outBuf, pcmWaiter);

    startDecode = xSemaphoreCreateBinary();

    namespace exec = std::execution;
    ctx0 = new (execCtx0Buf) cppos::exec_context(0);
    ctx1 = new (execCtx1Buf) cppos::exec_context(1);

    auto c0Thread = ctx0->get_scheduler<1, 4000>();
    auto c1Thread = ctx1->get_scheduler<2, 4800>();

    cppos::LoopExecutor c0Loop(c0Thread);
    cppos::LoopExecutor c1Loop(c1Thread);

    // Start everything
    cppos::startContexts();

    AudioServer srv(c0Loop, srvAddr, audioPort);
    OpusAudioDecoder decoder;
    auto soundDecode = [&decoder] (std::span<std::byte> frame)
    {
        return decoder.decode(frame);
    };

    auto waitForOpusQueue = [opusWaiter]
    {
        static bool decodeStarted = false;

        if (!decodeStarted)
        {
            xSemaphoreTake(startDecode, portMAX_DELAY);
            decodeStarted = true;
        }
        xQueueSelectFromSet(opusWaiter, portMAX_DELAY);
    };
    cppos::nullary_function waitOpus(waitForOpusQueue);

    auto waitForPcmQueue = [pcmWaiter]
    {
        xQueueSelectFromSet(pcmWaiter, portMAX_DELAY);
    };
    cppos::nullary_function waitPcm(waitForPcmQueue);

    static_assert(std::is_function_v<decltype(send2I2s)>);
    static_assert(std::is_pointer_v<std::decay_t<decltype(send2I2s)>>);
    static_assert(std::is_same_v<std::invoke_result_t<std::decay_t<decltype(send2I2s)>, std::span<std::byte>>, void>);

    srv.readPacket()
        | cppos::bufferedTransfer(inBuf, c1Loop.scheduler(&waitOpus))
        | exec::then(soundDecode)
        | cppos::bufferedTransfer(outBuf, c0Loop.scheduler(&waitPcm))
        | exec::then(send2I2s)
        | cppos::forEver();

    // never reached

    return ESP_OK;
}
