#define OVERRIDE_OPUS_ALLOC 1
#define OVERRIDE_OPUS_FREE 1
#define opus_alloc(x) NULL
#define opus_free(x) NULL
#define NONTHREADSAFE_PSEUDOSTACK 1
#define FIXED_POINT 1

#define HAVE_INTTYPES_H 1
#define HAVE_STDINT_H 1
#define HAVE_STDLIB_H 1
#define HAVE_STRING_H 1

/* Define to the sub-directory where libtool stores uninstalled libraries. */
#define LT_OBJDIR ".libs/"

/* This is a build of OPUS */
#define OPUS_BUILD /**/

/* Define to the address where bug reports for this package should be sent. */
//#define PACKAGE_BUGREPORT "opus@xiph.org"

/* Define to the full name of this package. */
//#define PACKAGE_NAME "opus"

/* Define to the full name and version of this package. */
//#define PACKAGE_STRING "opus 1.3.1"

/* Define to the one symbol short name of this package. */
//#define PACKAGE_TARNAME "opus"

/* Define to the home page for this package. */
//#define PACKAGE_URL ""

/* Define to the version of this package. */
//#define PACKAGE_VERSION "1.3.1"

/* Define to 1 if you have the ANSI C header files. */
#define STDC_HEADERS 1

/* Define to the equivalent of the C99 'restrict' keyword, or to
   nothing if this is not supported.  Do not define if restrict is
   supported directly.  */
//#define restrict __restrict
